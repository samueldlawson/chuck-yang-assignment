package com.samplecode.demo.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.samplecode.demo.model.Branch;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.util.ResourceUtils;

import java.util.List;

import static org.junit.Assert.*;

public class BranchUtilTest {
    ObjectMapper mapper = new ObjectMapper();
    JsonNode branchNode;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void parseHalifaxJson() throws Exception {
        String json = mapper.readTree(
                ResourceUtils.getFile(this.getClass().getResource("/body-with-two-branches.json")))
                .toString();

        List<Branch> list = BranchUtil.parseHalifaxJson(json);

        assertEquals(list.size(), 2);
    }

    @Test
    public void createBranchFromJsonNodeFromGoodFile() throws Exception {
        branchNode = mapper.readTree(ResourceUtils.getFile(this.getClass().getResource("/one-branch.json")));
        Branch branch = BranchUtil.createBranchFromJsonNode(branchNode);

        assertNotNull(branch);
        assertEquals(branch.getBranchName().toUpperCase(), "TADCASTER");
        assertEquals(branch.getCity(), "TADCASTER");
        assertEquals(branch.getCountry(), "GB");
        assertEquals(branch.getCountrySubDivision(), "NORTH YORKSHIRE");
        assertEquals(branch.getPostCode(), "LS24 9AL");
        assertEquals(branch.getStreetAddress(), "HALIFAX BRANCH 24 BRIDGE STREET");
        assertEquals(branch.getLatitude(), 53.884262F, .0001F);
        assertEquals(branch.getLongitude(), -1.261096F, .0001F);
    }

    @Test
    public void createBranchFromJsonNodeWithNoName() throws Exception {
        branchNode = mapper.readTree(
                ResourceUtils.getFile(this.getClass().getResource("/no-name-branch.json")));

        assertNull(BranchUtil.createBranchFromJsonNode(branchNode));
    }

    @Test
    public void createBranchFromJsonNodeWithFaultyData() throws Exception {
        branchNode = mapper.readTree(
                ResourceUtils.getFile(this.getClass().getResource("/faulty-branch.json")));

        Branch branch = BranchUtil.createBranchFromJsonNode(branchNode);
        assertNotNull(branch);

        assertEquals(branch.getBranchName().toUpperCase(), "TADCASTER");
        assertEquals(branch.getCity(), "MissingValue");
        assertNull(branch.getCountry());
        assertNull(branch.getCountrySubDivision());
        assertNull(branch.getPostCode());
        assertNull(branch.getStreetAddress());
        assertEquals(branch.getLatitude(), 0.0F, .0001F);
        assertEquals(branch.getLongitude(), -1.261096F, .0001F);
    }

    @Test
    public void createBranchFromJsonNodeWithFaultyData2() throws Exception {
        branchNode = mapper.readTree(ResourceUtils.getFile(this.getClass().getResource("/faulty-branch2.json")));

        Branch branch = BranchUtil.createBranchFromJsonNode(branchNode);
        assertNotNull(branch);
        assertEquals(branch.getBranchName().toUpperCase(), "TADCASTER");
        assertEquals(branch.getCity(), "Tadcaster");
        assertEquals(branch.getCountry(), "GB");
        assertEquals(branch.getCountrySubDivision(), "NORTH YORKSHIRE, CountrySubDivision LINE2");
        assertEquals(branch.getPostCode(), "LS24 9AL");
        assertEquals(branch.getStreetAddress(), "HALIFAX BRANCH 24 BRIDGE STREET, ADDR LINE2");
        assertEquals(branch.getLatitude(), 53.884262F, .0001F);
        assertEquals(branch.getLongitude(), 0.0F, .0001F);
    }

    @Test
    public void concatJsonNodeStringArray() throws Exception {
        String json = "[\"LONDON E1\",\"ct2\"]";
        JsonNode node = mapper.readTree(json);

        String str = BranchUtil.concatJsonNodeStringArray(node);

        assertEquals(str, "LONDON E1, ct2");
    }

    @Test
    public void filterByCity() throws Exception {
        String json = mapper.readTree(
                ResourceUtils.getFile(this.getClass().getResource("/body-with-two-branches.json")))
                .toString();

        List<Branch> list = BranchUtil.parseHalifaxJson(json);

        List<Branch> filteredList = BranchUtil.filterByCity(list, "MissingValue");
        assertEquals(filteredList.size(), 1);

        filteredList = BranchUtil.filterByCity(list, "TaDcAsTER");
        assertEquals(filteredList.size(), 1);

        filteredList = BranchUtil.filterByCity(list, "ABC");
        assertEquals(filteredList.size(), 0);
    }
}