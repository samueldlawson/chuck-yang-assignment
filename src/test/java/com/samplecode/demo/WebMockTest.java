package com.samplecode.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.samplecode.demo.client.HalifaxClient;
import com.samplecode.demo.model.Branch;
import com.samplecode.demo.rest.BranchLocatorController;
import com.samplecode.demo.service.BranchLocatorService;
import com.samplecode.demo.util.BranchUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.ResourceUtils;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(BranchLocatorController.class)
public class WebMockTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private HalifaxClient client;

    @MockBean
    private BranchLocatorService service;

    @Test
    public void endpointsWithMockedResponseBody() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String jsonString = mapper.readTree(
                ResourceUtils.getFile(this.getClass().getResource("/body-with-two-branches.json")))
                .toString();

        when(client.fetchData()).thenReturn(jsonString);
        List<Branch> branches = BranchUtil.parseHalifaxJson(client.fetchData());

        when(service.findAll()).thenReturn(branches);

        this.mockMvc.perform(get("/branches"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("TADCASTER")))
                .andExpect(content().string(containsString("MissingValue")));


        List<Branch> filteredBranches = BranchUtil.filterByCity(branches, "TADCASTER");

        when(service.findByCity("TADCASTER")).thenReturn(filteredBranches);
        this.mockMvc.perform(get("/branches/cities/TADCASTER"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("TADCASTER")))
                .andExpect(content().string(not(containsString("MissingValue"))));

        List<Branch> filteredBranches2 = BranchUtil.filterByCity(branches, "MissingValue");

        when(service.findByCity("MissingValue")).thenReturn(filteredBranches2);
        this.mockMvc.perform(get("/branches/cities/MissingValue"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("MissingValue")))
                .andExpect(content().string(not(containsString("TADCASTER"))));

    }
}
