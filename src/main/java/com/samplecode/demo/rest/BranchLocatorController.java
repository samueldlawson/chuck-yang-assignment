package com.samplecode.demo.rest;

import com.samplecode.demo.model.Branch;
import com.samplecode.demo.service.BranchLocatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BranchLocatorController {

    @Autowired
    BranchLocatorService service;

    @GetMapping("/branches")
    public List<Branch> getAllBranches() {
        return service.findAll();
    }

    @GetMapping("/branches/cities/{city}")
    public List<Branch> getBranchesByCity(@PathVariable String city) {
        System.out.println("city = " + city);
        return service.findByCity(city);
    }
}
