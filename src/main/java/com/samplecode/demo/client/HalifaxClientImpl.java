package com.samplecode.demo.client;

import com.samplecode.demo.model.Branch;
import com.samplecode.demo.util.BranchUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

@Service
public class HalifaxClientImpl implements HalifaxClient {

    private RestTemplate restTemplate = new RestTemplate();

    @Value("${url.halifax}")
    private String urlHalifax;

    @Override
    public String fetchData() {
        try {
            ResponseEntity<String> response = restTemplate.getForEntity(URI.create(urlHalifax), String.class);
            System.out.println("got entity from Halifax");

            return response.getBody();

        } catch (RestClientResponseException e) {
            System.out.println(String.format("Error code %d : %s", e.getRawStatusCode(), e.getResponseBodyAsString()));
        }

        return null;    // Only after exception thrown
    }
}
