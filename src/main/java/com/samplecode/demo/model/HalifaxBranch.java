package com.samplecode.demo.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class HalifaxBranch {
    public final String identification;
    public final String sequenceNumber;
    public final String name;
    public final String type;
    public final String[] customerSegment;
    public final OtherServiceAndFacility otherServiceAndFacility[];
    public final Availability availability;
    public final ContactInfo contactInfo[];
    public final PostalAddress postalAddress;
    public final String[] accessibility;

    @JsonCreator
    public HalifaxBranch(@JsonProperty("Identification") String identification,
                         @JsonProperty("SequenceNumber") String sequenceNumber,
                         @JsonProperty("Name") String name,
                         @JsonProperty("Type") String type,
                         @JsonProperty("CustomerSegment") String[] customerSegment,
                         @JsonProperty("Accessibility") String[] accessibility,
                         @JsonProperty("OtherServiceAndFacility") OtherServiceAndFacility[] otherServiceAndFacility,
                         @JsonProperty("Availability") Availability availability,
                         @JsonProperty("ContactInfo") ContactInfo[] contactInfo,
                         @JsonProperty("PostalAddress") PostalAddress postalAddress) {
        this.identification = identification;
        this.sequenceNumber = sequenceNumber;
        this.name = name;
        this.type = type;
        this.customerSegment = customerSegment;
        this.accessibility = accessibility;
        this.otherServiceAndFacility = otherServiceAndFacility;
        this.availability = availability;
        this.contactInfo = contactInfo;
        this.postalAddress = postalAddress;
    }

    public static final class OtherServiceAndFacility {
        public final String code;
        public final String name;
        public final String description;

        @JsonCreator
        public OtherServiceAndFacility(@JsonProperty("Code") String code,
                                       @JsonProperty("Name") String name,
                                       @JsonProperty("Description") String description) {
            this.code = code;
            this.name = name;
            this.description = description;
        }
    }

    public static final class Availability {
        public final StandardAvailability standardAvailability;

        @JsonCreator
        public Availability(@JsonProperty("StandardAvailability") StandardAvailability standardAvailability) {
            this.standardAvailability = standardAvailability;
        }

        public static final class StandardAvailability {
            public final Day day[];

            @JsonCreator
            public StandardAvailability(@JsonProperty("Day") Day[] day) {
                this.day = day;
            }

            public static final class Day {
                public final String name;
                public final OpeningHour openingHours[];

                @JsonCreator
                public Day(@JsonProperty("Name") String name,
                           @JsonProperty("OpeningHours") OpeningHour[] openingHours) {
                    this.name = name;
                    this.openingHours = openingHours;
                }

                public static final class OpeningHour {
                    public final String openingTime;
                    public final String closingTime;

                    @JsonCreator
                    public OpeningHour(@JsonProperty("OpeningTime") String openingTime,
                                       @JsonProperty("ClosingTime") String closingTime) {
                        this.openingTime = openingTime;
                        this.closingTime = closingTime;
                    }
                }
            }
        }
    }

    public static final class ContactInfo {
        public final String contactType;
        public final String contactContent;

        @JsonCreator
        public ContactInfo(@JsonProperty("ContactType") String contactType,
                           @JsonProperty("ContactContent") String contactContent) {
            this.contactType = contactType;
            this.contactContent = contactContent;
        }
    }

    public static final class PostalAddress {
        public final String[] addressLine;
        public final String townName;
        public final String[] countrySubDivision;
        public final String country;
        public final String postCode;
        public final GeoLocation geoLocation;

        @JsonCreator
        public PostalAddress(@JsonProperty("AddressLine") String[] addressLine,
                             @JsonProperty("TownName") String townName,
                             @JsonProperty("CountrySubDivision") String[] countrySubDivision,
                             @JsonProperty("Country") String country,
                             @JsonProperty("PostCode") String postCode,
                             @JsonProperty("GeoLocation") GeoLocation geoLocation) {
            this.addressLine = addressLine;
            this.townName = townName;
            this.countrySubDivision = countrySubDivision;
            this.country = country;
            this.postCode = postCode;
            this.geoLocation = geoLocation;
        }

        public static final class GeoLocation {
            public final GeographicCoordinates geographicCoordinates;

            @JsonCreator
            public GeoLocation(@JsonProperty("GeographicCoordinates") GeographicCoordinates geographicCoordinates) {
                this.geographicCoordinates = geographicCoordinates;
            }

            public static final class GeographicCoordinates {
                public final String latitude;
                public final String longitude;

                @JsonCreator
                public GeographicCoordinates(@JsonProperty("Latitude") String latitude,
                                             @JsonProperty("Longitude") String longitude) {
                    this.latitude = latitude;
                    this.longitude = longitude;
                }
            }
        }
    }
}