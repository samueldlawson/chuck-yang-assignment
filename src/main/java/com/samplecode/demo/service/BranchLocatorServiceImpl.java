package com.samplecode.demo.service;

import com.samplecode.demo.client.HalifaxClient;
import com.samplecode.demo.model.Branch;
import com.samplecode.demo.util.BranchUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BranchLocatorServiceImpl implements BranchLocatorService {

    @Autowired
    HalifaxClient halifax;

    @Override
    public List<Branch> findAll() {
        return BranchUtil.parseHalifaxJson(halifax.fetchData());
    }

    @Override
    public List<Branch> findByCity(String city) {
        return BranchUtil.filterByCity(findAll(), city);
    }
}
