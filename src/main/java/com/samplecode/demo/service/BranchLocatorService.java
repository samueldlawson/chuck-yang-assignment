package com.samplecode.demo.service;

import com.samplecode.demo.model.Branch;

import java.util.List;

public interface BranchLocatorService {
    List<Branch> findAll();

    List<Branch> findByCity(String city);
}
