package com.samplecode.demo.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.samplecode.demo.model.Branch;

import java.util.ArrayList;
import java.util.List;

public class BranchUtil {

    public static List<Branch> parseHalifaxJson(String jsonString) {
        List<Branch> branches = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode dataNodes = null;

        System.out.println("parsing json...");
        try {
            JsonNode root = mapper.readTree(jsonString);

            int total = root.get("meta").get("TotalResults").asInt();
            System.out.println("TotalResults: " + total);

            dataNodes = root.findPath("Branch");
        } catch (Exception e) {
            System.out.println("Error parsing halifax client response");
        }

        if (dataNodes != null && !dataNodes.isMissingNode())
            for (JsonNode node : dataNodes) {
                Branch b = createBranchFromJsonNode(node);
                if (b == null) continue;

                branches.add(b);
            }

        return branches;
    }

    public static Branch createBranchFromJsonNode(JsonNode node) {
        Branch b = new Branch();

        if (node.get("Name") == null || node.get("Name").isMissingNode()) {
            System.out.println("JsonNode is missing branch name");
            System.out.println(" \t " + node.toString());
            System.out.println(" \t skipping");
            return null;
        }

        b.setBranchName(node.get("Name").asText());

        try {
            if (!node.findPath("Latitude").isMissingNode())
                try {
                    b.setLatitude(Float.parseFloat(node.findPath("Latitude").asText().trim()));
                } catch (Exception e) {
                    System.out.println("Error parsing float, latitude = " + node.findPath("Latitude").asText().trim());
                }

            if (!node.findPath("Longitude").isMissingNode())
                try {
                    b.setLongitude(Float.parseFloat(node.findPath("Longitude").asText().trim()));
                } catch (Exception e) {
                    System.out.println("Error parsing float, longitude = " + node.findPath("Longitude").asText().trim());
                }

            if (!node.findPath("AddressLine").isMissingNode()) {
                b.setStreetAddress(concatJsonNodeStringArray(node.findPath("AddressLine")));
            }

            if (!node.findPath("TownName").isMissingNode())
                b.setCity(node.findPath("TownName").asText());
            else
                b.setCity("MissingValue");

            if (!node.findPath("CountrySubDivision").isMissingNode()) {
                b.setCountrySubDivision(concatJsonNodeStringArray(node.findPath("CountrySubDivision")));
            }

            if (!node.findPath("Country").isMissingNode())
                b.setCountry(node.findPath("Country").asText());

            if (!node.findPath("PostCode").isMissingNode())
                b.setPostCode(node.findPath("PostCode").asText());

        } catch (Exception e) {
            System.out.println("Error processing Halifax branch ");
            System.out.println("\t " + node.toString());
        }
        return b;
    }

    public static String concatJsonNodeStringArray(JsonNode node) {
        if (node == null && !node.isMissingNode())
            return null;
        else if (node.isArray()) {
            String temp = node.toString().replaceAll("\",\"", ", ");
            return temp.substring(2, temp.length() - 2);
        } else {
            return node.toString();
        }

    }

    public static List<Branch> filterByCity(List<Branch> all, String city) {
        System.out.println("helper filtering... city = " + city);
        List<Branch> list = new ArrayList<>();
        if (all != null)
            for (Branch branch : all) {
                if (branch.getCity().equalsIgnoreCase(city))
                    list.add(branch);
            }

        return list;
    }

}
