# Aavri Demo

This is the Aavri take home assessment

----


## Objective 

> To create a RESTful web service which a frontend developer could use 
to display branch locations in a customer-facing web application. 
The application should obtain the list of branches from Halifax’s 
open banking API, perform the necessary transformations on the data, 
and make it accessible to clients calling the API.

----


## Out of Scope
• No authentication or other security mechanisms
• No data needs to be persisted
• No other searching or filtering is required

----


## Endpoints

This demo project provide 2 GET endpoints 

1.  /branches
get all bank branches

1.  /branches/cities/-mycity-
get all bank branches with city name "-mycity-" (case ignored)

----


## Model

#### Branch data Model:
-   branchName      String
-   latitude        loat
-   longitude       float
-   streetAddress   String
-   city            String
-   countrySubDivision  String
-   country         String
-   postCode        String

#### Data Format
Json is required

#### Sample output
```javascript 
[
    {
      "branchName": "TADCASTER",
      "latitude": 53.884262,
      "longitude": -1.261096,
      "streetAddress": "\"HALIFAX BRANCH 24 BRIDGE STREET",
      "city": "TADCASTER",
      "countrySubDivision": "\"NORTH YORKSHIRE",
      "country": "GB",
      "postCode": "LS24 9AL"
    },
    {
      "branchName": "WALTON VALE",
      "latitude": 53.46301,
      "longitude": -2.959379,
      "streetAddress": "\"HALIFAX BRANCH 78-80 WALTON VALE",
      "city": "LIVERPOOL",
      "countrySubDivision": "\"MERSEYSIDE",
      "country": "GB",
      "postCode": "L9 2BU"
    }
]
```

----


## Notes

### Halifax Data
We don't have the data model source code for Halifax response body, 
or they model their branches exactly.

#### Observations
1. They do have wrappers (with meta data, and Brand) around branches 
data in response body.
1. Their API seems only provide all branches data, and we have to 
transfer and/or filter data per our Branch data model. 
1. They seems to have only ONE BrandName (Halifax) available.  
All branches are under this collection umbrella.

#### Reference Documentation
* Source Data URL:
https://api.halifax.co.uk/opendata-v2.2/branch

* Source Data Documentation:
https://developer.halifax.co.uk/opendata-v2.2

----


### Implementation details
1. Halifax url is configured in application.properties 

1. Heuristic Matching:
-   branchName          ->  Top level field "Name"
-   latitude            ->  "Latitude"
-   longitude           ->  "Longitude"
-   streetAddress       ->  "AddressLine"
-   city                ->  "TownName"
-   countrySubDivision  ->  "CountrySubDivision"
-   country             ->  "Country"
-   postCode            ->  "PostCode"    

1. After heuristic matching, there are still inconsistent data 
(2 records for now) with no obvious meaningful "city" to match.

1. In this implementation, we do require the top level "Name" 
in Halifax branch data to match the branchName.
If not, the record would be skipped 
(after logging the record's json string to console)

1. If a Halifax branch does have a top level "Name" field, 
all other fields are _optional_ for now.
If there are none matching the keyword, 
we either leave the field as null or fill with string "MissingValue" 
in the case for city/"TownName"

1. We use jackson parsing json data, specifically using findPath("key") 
to reach JsonNode data.  It stops at the first match, but serves for our 
purpose for now. 

1. The code is also flexible to cope with minor Halifax data model 
changes, as long as they keep our keywords there, 
and unique except for top level branch "Name". 

1. packages 
* com.samplecode.demo

    for applications and configurations
    
* com.samplecode.demo.client
    
    for client code for Halifax

* com.samplecode.demo.model
    
    for data models

* com.samplecode.demo.rest
    
    for RESTful web services

* com.samplecode.demo.service
    
    for service interface and implementation

* com.samplecode.demo.util
    
    for helper utilities

----


## Build
> mvn clean install

----


## Run

1. start embedded web server (maven-3 and java-8 required)
> java -jar target/demo-0.0.1-SNAPSHOT.jar

1. in browser query the endpoints with urls:
http://localhost:8080/branches
http://localhost:8080/branches/kingsbury




